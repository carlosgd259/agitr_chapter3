// This program publishes randomly-generated velocity
// messages for turtlesim.
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>  // For geometry_msgs::Twist
#include <stdlib.h> // For rand() and RAND_MAX
#include <turtlesim/Pose.h>
#include <iomanip> // for std::setprecision and std::fixed

float posx;
float posy;


void poseMessageReceived(const turtlesim::Pose& msg) {
  posx = msg.x;
  posy = msg.y;
 
}



int main(int argc, char **argv) {


  // Initialize the ROS system and become a node.
  ros::init(argc, argv, "publish_velocity");
  ros::NodeHandle nh;

  // Create a publisher object.
  ros::Publisher pub = nh.advertise<geometry_msgs::Twist>(
    "turtle1/cmd_vel", 1000);

    // Create a subscriber object.
  ros::Subscriber sub = nh.subscribe("turtle1/pose", 1000,
    &poseMessageReceived);

  // Seed the random number generator.
  srand(time(0));

  // Loop at 2Hz until the node is shut down.
  ros::Rate rate(2);
  while(ros::ok()) {
    // Create and fill in the message.  The other four
    // fields, which are ignored by turtlesim, default to 0.
    geometry_msgs::Twist msg;

    if((posx >= 3) && (posx <= 8) && (posy >= 3) && (posy <= 8)){
      msg.linear.x = 1.0;
      msg.angular.z = 2*double(rand())/double(RAND_MAX) - 1;
    }

    else if((posx >= 11) || (posx <= 0) || (posy >= 11) || (posy <= 0)) {
      msg.linear.x = 1.0;
      msg.angular.z = 1.0;
    }
    else{
      msg.linear.x = double(rand())/double(RAND_MAX);
      msg.angular.z = 2*double(rand())/double(RAND_MAX) - 1;}

    // Publish the message.
    pub.publish(msg);

    // Send a message to rosout with the details.
    ROS_INFO_STREAM("Sending random velocity command:"
      << " linear=" << msg.linear.x
      << " Aangular=" << msg.angular.z
      << "position=(" <<  posx << "," << posy << ")");

    // Wait until it's time for another iteration.
    rate.sleep();
    ros::spinOnce();
  }

  // Let ROS take over.
  

}
